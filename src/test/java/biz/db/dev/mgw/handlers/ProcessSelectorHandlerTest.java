package biz.db.dev.mgw.handlers;

import biz.db.dev.mgw.builders.SmtpResponse;
import biz.db.dev.mgw.builders.SmtpResponseLine;
import biz.db.dev.mgw.config.SmtpServerConfigurationImpl;
import biz.db.dev.mgw.enums.SmtpResponseCode;
import biz.db.dev.mgw.smtp.SmtpRequest;
import biz.db.dev.mgw.smtp.SmtpSession;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.equalTo;

public class ProcessSelectorHandlerTest {

    private SmtpServerConfigurationImpl smtpServerConfiguration;

    private ProcessSelectorHandler handler;

    @Before
    public void init() {
        this.smtpServerConfiguration = new SmtpServerConfigurationImpl();

        this.smtpServerConfiguration.setHostname("mail.test.com");
        this.smtpServerConfiguration.setPort(25);
        this.smtpServerConfiguration.setMailStoreDirectory("build/tmp");

        SmtpSession session = new SmtpSession(smtpServerConfiguration);

        this.handler = new ProcessSelectorHandler(session);
    }

    @Test
    public void testHandshake() {

        SmtpRequest request = new SmtpRequest("EHLO smtp.client.org", "EHLO", "smtp.client.org");

        SmtpResponse response = this.handler.onCommand(request);

        assertThat(response.getLines().size(), is(equalTo(1)));

        SmtpResponseLine line = response.getLines().get(0);
        assertThat(line.getCode(), is(equalTo(SmtpResponseCode.OK)));
        assertThat(line.getText(), is(equalTo("Hello smtp.client.org, I am glad to meet you")));
    }
}
