package biz.db.dev.mgw.services.impl;

import biz.db.dev.mgw.config.SmtpServerConfiguration;
import biz.db.dev.mgw.services.SmtpServerHandlerFactory;
import biz.db.dev.mgw.smtp.SmtpSession;
import io.netty.channel.SimpleChannelInboundHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class SmtpServerHandlerFactoryImpl implements SmtpServerHandlerFactory {

    private final SmtpServerConfiguration smtpServerConfiguration;

    @Autowired
    public SmtpServerHandlerFactoryImpl(SmtpServerConfiguration smtpServerConfiguration) {
        Objects.requireNonNull(smtpServerConfiguration);

        this.smtpServerConfiguration = smtpServerConfiguration;
    }

    @Override
    public SimpleChannelInboundHandler<String> newInstance() {
        SmtpSession session = new SmtpSession(this.smtpServerConfiguration);

        return new SmtpServerHandlerImpl(session);
    }
}
