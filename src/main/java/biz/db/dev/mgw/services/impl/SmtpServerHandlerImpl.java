package biz.db.dev.mgw.services.impl;

import biz.db.dev.mgw.builders.SmtpResponse;
import biz.db.dev.mgw.handlers.CommandHandler;
import biz.db.dev.mgw.handlers.ProcessSelectorHandler;
import biz.db.dev.mgw.smtp.SmtpRequest;
import biz.db.dev.mgw.smtp.SmtpSession;
import biz.db.dev.mgw.utils.SmtpRequestParser;
import biz.db.dev.mgw.utils.SmtpResponseUtils;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

public class SmtpServerHandlerImpl extends SimpleChannelInboundHandler<String> {

    private static final Logger logger = LoggerFactory.getLogger(SmtpServerHandlerImpl.class);
    private static final Logger smtpLogger = LoggerFactory.getLogger("smtp.inbound");

    private static final String WELCOME_MESSAGE = "220 %s ESMTP\r\n";

    private final SmtpSession session;

    private CommandHandler commandHandler;

    public SmtpServerHandlerImpl(SmtpSession session) {
        Objects.requireNonNull(session);

        this.session = session;

        this.commandHandler = new ProcessSelectorHandler(this.session);
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        // Send greeting for a new connection.
        ctx.write(String.format(WELCOME_MESSAGE, this.session.getSmtpServerConfiguration().getHostname()));
        ctx.flush();
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        // do not need it at the moment
    }

    @Override
    public void channelRead0(ChannelHandlerContext ctx, String requestLine) throws Exception {
        if(smtpLogger.isDebugEnabled()) {
            smtpLogger.debug("'{}'", requestLine, ctx.name());
        }

        SmtpRequest request = SmtpRequestParser.parse(requestLine);
        SmtpResponse response = this.commandHandler.onCommand(request);

        SmtpResponseUtils.writeResponse(this.session, response, ctx);
        this.commandHandler = response.getCommandHandler();
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        logger.warn("Exception caught while processing incoming connection", cause);

        ctx.close();
    }
}