package biz.db.dev.mgw.services.impl;

import biz.db.dev.mgw.config.SmtpServerConfigurationImpl;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Objects;

@Service
public class SmtpServerImpl {

    private EventLoopGroup bossGroup;
    private EventLoopGroup workerGroup;

    private final SmtpServerInitializerImpl serverInitializer;

    private final SmtpServerConfigurationImpl serverConfiguration;

    @Autowired
    public SmtpServerImpl(SmtpServerInitializerImpl serverInitializer, SmtpServerConfigurationImpl serverConfiguration) {
        Objects.requireNonNull(serverInitializer);
        Objects.requireNonNull(serverConfiguration);

        this.serverInitializer = serverInitializer;
        this.serverConfiguration = serverConfiguration;
    }


    @PostConstruct
    public void bootstrapServer() throws InterruptedException {
        this.bossGroup = new NioEventLoopGroup(1);
        this.workerGroup = new NioEventLoopGroup();

        ServerBootstrap bootstrap = new ServerBootstrap();

        bootstrap
                .group(this.bossGroup, this.workerGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(this.serverInitializer);

        bootstrap
                .bind(serverConfiguration.getPort())
                .sync()
                .channel()
                .closeFuture()
                .sync();
    }

    @PreDestroy
    public void shutdown() {
        bossGroup.shutdownGracefully();
        workerGroup.shutdownGracefully();
    }
}
