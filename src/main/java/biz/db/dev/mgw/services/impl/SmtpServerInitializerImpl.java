package biz.db.dev.mgw.services.impl;

import biz.db.dev.mgw.services.SmtpServerHandlerFactory;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SmtpServerInitializerImpl extends ChannelInitializer<SocketChannel> {

    private final List<SmtpServerHandlerFactory> smtpServerHandlerFactory;

    @Autowired
    public SmtpServerInitializerImpl(List<SmtpServerHandlerFactory> smtpServerHandlerFactory) {
        this.smtpServerHandlerFactory = smtpServerHandlerFactory;
    }

    @Override
    public void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();

        // Add the text line codec combination first,
        pipeline.addLast(new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()));
        // the encoder and decoder are static as these are sharable
        pipeline.addLast(new StringDecoder());
        pipeline.addLast(new StringEncoder());

        // and then business logic.

        for( SmtpServerHandlerFactory handler : this.smtpServerHandlerFactory) {
            pipeline.addLast(handler.newInstance());
        }
    }
}
