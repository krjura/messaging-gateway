package biz.db.dev.mgw.services;

import io.netty.channel.SimpleChannelInboundHandler;

public interface SmtpServerHandlerFactory {

    SimpleChannelInboundHandler<String> newInstance();
}
