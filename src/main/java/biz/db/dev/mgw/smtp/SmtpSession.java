package biz.db.dev.mgw.smtp;

import biz.db.dev.mgw.config.SmtpServerConfiguration;

import java.util.Objects;

public class SmtpSession {

    private final SmtpServerConfiguration smtpServerConfiguration;

    private final SmtpServerLogger serverLogger;

    public SmtpSession(SmtpServerConfiguration smtpServerConfiguration) {
        Objects.requireNonNull(smtpServerConfiguration);

        this.smtpServerConfiguration = smtpServerConfiguration;
        this.serverLogger = new SmtpServerLogger(this.smtpServerConfiguration);
        this.serverLogger.init();
    }

    public SmtpServerConfiguration getSmtpServerConfiguration() {
        return smtpServerConfiguration;
    }

    public SmtpServerLogger getServerLogger() {
        return serverLogger;
    }
}
