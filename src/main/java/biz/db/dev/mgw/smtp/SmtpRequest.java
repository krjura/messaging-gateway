package biz.db.dev.mgw.smtp;

import java.util.Objects;

public class SmtpRequest {

    private String requestLine;

    private String command;

    private String parameters;

    public SmtpRequest(String requestLine, String command, String parameters) {
        Objects.requireNonNull(requestLine);
        Objects.requireNonNull(command);

        this.requestLine = requestLine;
        this.command = command;
        this.parameters = parameters;
    }

    public String getRequestLine() {
        return requestLine;
    }

    public String getCommand() {
        return command;
    }

    public String getParameters() {
        return parameters;
    }
}
