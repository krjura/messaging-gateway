package biz.db.dev.mgw.smtp;

import biz.db.dev.mgw.config.SmtpServerConfiguration;
import biz.db.dev.mgw.services.impl.SmtpServerHandlerImpl;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.atomic.AtomicInteger;

public class SmtpServerLogger {

    private static final Logger logger = LoggerFactory.getLogger(SmtpServerHandlerImpl.class);

    private static final AtomicInteger globalCounter = new AtomicInteger(1);

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

    private static final String SEPARATOR = "-";
    private static final String INCOMING_PREFIX = ">";
    private static final String OUTGOING_PREFIX = "<";
    private static final String NEW_LINE = "\n";
    private static final String FILENAME_EXTENSION = ".eml";

    private SmtpServerConfiguration smtpServerConfiguration;

    private String serverLogFilename;

    public SmtpServerLogger(SmtpServerConfiguration smtpServerConfiguration) {
        this.smtpServerConfiguration = smtpServerConfiguration;
    }

    public void init() {
        if( this.serverLogFilename != null) {
            return;
        }

        String datePart = FORMATTER.format(ZonedDateTime.now());
        String hostnamePart = Hex.encodeHexString(this.smtpServerConfiguration.getHostname().getBytes());

        this.serverLogFilename = smtpServerConfiguration.getMailStoreDirectory() +
                File.separator +
                datePart +
                SEPARATOR +
                hostnamePart +
                SEPARATOR +
                globalCounter.getAndIncrement() +
                FILENAME_EXTENSION;
    }

    private BufferedWriter openWriter() throws IOException {
        return new BufferedWriter(new FileWriter(this.serverLogFilename));
    }

    public void writeServerIncomingTraffic(String line) {
        try( BufferedWriter serverWriter = openWriter() ) {

            serverWriter.write(INCOMING_PREFIX);
            serverWriter.write(line);
            serverWriter.write(NEW_LINE);

            serverWriter.flush();
        } catch (IOException e) {
            logger.warn("Cannot write to server log", e);
        }
    }

    public void writeServerOutgoingTraffic(String line) {
        try( BufferedWriter serverWriter = openWriter() ) {
            serverWriter.write(OUTGOING_PREFIX);
            serverWriter.write(line);
            serverWriter.write("\n");

            serverWriter.flush();
        } catch (IOException e) {
            logger.warn("Cannot write to server log", e);
        }
    }
}
