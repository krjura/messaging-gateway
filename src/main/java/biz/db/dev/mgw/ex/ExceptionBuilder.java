package biz.db.dev.mgw.ex;

public final class ExceptionBuilder {

    private ExceptionBuilder() {

    }

    public static SmtpServerException unknownCommandReceived() {
        return new SmtpServerException("unknown command received");
    }
}
