package biz.db.dev.mgw.handlers;

import biz.db.dev.mgw.builders.SmtpResponse;
import biz.db.dev.mgw.builders.SmtpResponseBuilder;
import biz.db.dev.mgw.enums.SmtpResponseCode;
import biz.db.dev.mgw.ex.ExceptionBuilder;
import biz.db.dev.mgw.smtp.SmtpRequest;
import biz.db.dev.mgw.smtp.SmtpSession;

public class MailCommandHandler extends BasicCommandHandler {

    private static final String MAIL_COMMAND = "MAIL";

    private CommandHandler parent;

    MailCommandHandler(SmtpSession session, CommandHandler parent) {
        super(session);

        this.parent = parent;
    }

    @Override
    public SmtpResponse processCommand(SmtpRequest request) {
        if( ! request.getCommand().equals(MAIL_COMMAND)) {
            throw ExceptionBuilder.unknownCommandReceived();
        }

        return SmtpResponseBuilder
                .newCommand()
                .commandHandler(this.parent)
                .line()
                    .code(SmtpResponseCode.OK)
                    .text("OK")
                    .crlf()
                    .build()
                .build();
    }
}