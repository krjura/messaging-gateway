package biz.db.dev.mgw.handlers;

import biz.db.dev.mgw.builders.SmtpResponse;
import biz.db.dev.mgw.builders.SmtpResponseBuilder;
import biz.db.dev.mgw.smtp.SmtpRequest;
import biz.db.dev.mgw.smtp.SmtpSession;

public class HelloProcessCommandHandler implements CommandHandler{

    private CommandHandler commandNotRecognizedHandler;

    HelloProcessCommandHandler(SmtpSession session) {
        this.commandNotRecognizedHandler = new CommandNotRecognizedCommandHandler(session, this);
    }

    @Override
    public SmtpResponse onCommand(SmtpRequest request) {
        return SmtpResponseBuilder
                .newCommand()
                .commandHandler(this.commandNotRecognizedHandler)
                .build();
    }
}
