package biz.db.dev.mgw.handlers;

import biz.db.dev.mgw.builders.SmtpResponse;
import biz.db.dev.mgw.smtp.SmtpRequest;

public interface CommandHandler {

    SmtpResponse onCommand(SmtpRequest request);
}
