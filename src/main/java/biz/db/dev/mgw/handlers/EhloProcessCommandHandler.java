package biz.db.dev.mgw.handlers;

import biz.db.dev.mgw.builders.SmtpResponse;
import biz.db.dev.mgw.smtp.SmtpRequest;
import biz.db.dev.mgw.smtp.SmtpSession;

import java.util.HashMap;
import java.util.Map;

public class EhloProcessCommandHandler implements CommandHandler{

    private static final String EHLO_COMMAND = "EHLO";
    private static final String MAIL_COMMAND = "MAIL";
    private static final String RCPT_COMMAND = "RCPT";
    private static final String QUIT_COMMAND = "QUIT";
    private static final String DATA_COMMAND = "DATA";

    private Map<String, CommandHandler> commandHandlers;

    private CommandHandler commandNotRecognizedHandler;

    EhloProcessCommandHandler(SmtpSession session) {
        this.commandHandlers = new HashMap<>();

        this.commandNotRecognizedHandler = new CommandNotRecognizedCommandHandler(session, this);

        this.commandHandlers.put(EHLO_COMMAND, new EhloCommandHandler(session, this));
        this.commandHandlers.put(MAIL_COMMAND, new MailCommandHandler(session, this));
        this.commandHandlers.put(RCPT_COMMAND, new RcptCommandHandler(session, this));
        this.commandHandlers.put(QUIT_COMMAND, new QuitCommandHandler(session, this));
        this.commandHandlers.put(DATA_COMMAND, new DataCommandHandler(session, this));
    }

    @Override
    public SmtpResponse onCommand(SmtpRequest request) {
        CommandHandler commandHandler = this
                .commandHandlers
                .getOrDefault(request.getCommand(), this.commandNotRecognizedHandler);

        return commandHandler.onCommand(request);
    }
}
