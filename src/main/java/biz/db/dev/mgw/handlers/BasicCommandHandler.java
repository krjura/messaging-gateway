package biz.db.dev.mgw.handlers;

import biz.db.dev.mgw.builders.SmtpResponse;
import biz.db.dev.mgw.smtp.SmtpRequest;
import biz.db.dev.mgw.smtp.SmtpSession;

public abstract class BasicCommandHandler implements CommandHandler {

    private final SmtpSession session;

    public BasicCommandHandler(SmtpSession session) {
        this.session = session;
    }

    @Override
    public SmtpResponse onCommand(SmtpRequest request) {

        this.session.getServerLogger().writeServerIncomingTraffic(request.getRequestLine());

        return processCommand(request);
    }

    abstract SmtpResponse processCommand(SmtpRequest command);
}
