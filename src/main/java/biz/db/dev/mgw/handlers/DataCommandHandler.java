package biz.db.dev.mgw.handlers;

import biz.db.dev.mgw.builders.SmtpResponse;
import biz.db.dev.mgw.builders.SmtpResponseBuilder;
import biz.db.dev.mgw.enums.SmtpResponseCode;
import biz.db.dev.mgw.ex.ExceptionBuilder;
import biz.db.dev.mgw.smtp.SmtpRequest;
import biz.db.dev.mgw.smtp.SmtpSession;

public class DataCommandHandler extends BasicCommandHandler {

    private static final String DATA_COMMAND = "DATA";

    private CommandHandler parent;

    private boolean dataMode = false;

    DataCommandHandler(SmtpSession session, CommandHandler parent) {
        super(session);

        this.parent = parent;
    }

    @Override
    public SmtpResponse processCommand(SmtpRequest command) {
        if( dataMode ) {
            return processData(command);
        } else {
            return startDataMode(command);
        }
    }

    private SmtpResponse processData(SmtpRequest request) {
        if( ".".equals(request.getCommand())) {
            return SmtpResponseBuilder
                    .newCommand()
                    .commandHandler(this.parent)
                    .line()
                        .code(SmtpResponseCode.OK)
                        .text("OK")
                        .crlf()
                        .build()
                    .build();
        } else {
            return SmtpResponseBuilder
                    .newCommand()
                    .commandHandler(this)
                    .build();
        }
    }

    private SmtpResponse startDataMode(SmtpRequest request) {

        if( ! request.getCommand().equals(DATA_COMMAND)) {
            throw ExceptionBuilder.unknownCommandReceived();
        }

        this.dataMode = true;

        return SmtpResponseBuilder
                .newCommand()
                .commandHandler(this)
                .line()
                    .code(SmtpResponseCode.START_MAIL_INPUT)
                    .text("Start mail input; end with <CRLF>.<CRLF>")
                    .crlf()
                    .build()
                .build();
    }
}
