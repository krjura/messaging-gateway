package biz.db.dev.mgw.handlers;

import biz.db.dev.mgw.builders.SmtpResponse;
import biz.db.dev.mgw.builders.SmtpResponseBuilder;
import biz.db.dev.mgw.enums.SmtpResponseCode;
import biz.db.dev.mgw.smtp.SmtpRequest;
import biz.db.dev.mgw.smtp.SmtpSession;

public class CommandNotRecognizedCommandHandler extends BasicCommandHandler {

    private CommandHandler parent;

    CommandNotRecognizedCommandHandler(SmtpSession session, CommandHandler parent) {
        super(session);

        this.parent = parent;
    }

    @Override
    public SmtpResponse processCommand(SmtpRequest request) {
        return SmtpResponseBuilder
                .newCommand()
                .commandHandler(this.parent)
                    .line()
                    .code(SmtpResponseCode.SYNTAX_ERROR)
                    .text("command not recognised")
                    .crlf()
                    .build()
                .build();
    }
}
