package biz.db.dev.mgw.handlers;

import biz.db.dev.mgw.builders.SmtpResponse;
import biz.db.dev.mgw.smtp.SmtpRequest;
import biz.db.dev.mgw.smtp.SmtpSession;

public class ProcessSelectorHandler implements CommandHandler {

    private static final String EHLO_COMMAND = "EHLO";
    private static final String HELLO_COMMAND = "HELLO";

    private SmtpSession session;

    public ProcessSelectorHandler(SmtpSession session) {
        this.session = session;
    }

    @Override
    public SmtpResponse onCommand(SmtpRequest request) {
        String command = request.getCommand();

        CommandHandler commandHandler;

        switch (command) {
            case EHLO_COMMAND:
                commandHandler = new EhloProcessCommandHandler(this.session);
                break;
            case HELLO_COMMAND:
                commandHandler = new HelloProcessCommandHandler(this.session);
                break;
            default:
                commandHandler = new CommandNotRecognizedCommandHandler(this.session, this);
                break;
        }

        return commandHandler.onCommand(request);
    }
}
