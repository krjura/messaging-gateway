package biz.db.dev.mgw.config;

public interface SmtpServerConfiguration {

    Integer getPort();

    String getHostname();

    String getMailStoreDirectory();
}
