package biz.db.dev.mgw.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "smtp-server")
public class SmtpServerConfigurationImpl implements SmtpServerConfiguration {

    private Integer port;

    private String hostname;

    private String mailStoreDirectory;

    @Override
    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    @Override
    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    @Override
    public String getMailStoreDirectory() {
        return mailStoreDirectory;
    }

    public void setMailStoreDirectory(String mailStoreDirectory) {
        this.mailStoreDirectory = mailStoreDirectory;
    }
}
