package biz.db.dev.mgw.builders;

import biz.db.dev.mgw.enums.SmtpResponseCode;
import biz.db.dev.mgw.handlers.CommandHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SmtpResponseBuilder {

    private List<SmtpResponseLine> lines;

    private CommandHandler commandHandler;

    private SmtpResponseBuilder() {
        this.lines = new ArrayList<>(1);
    }

    public static SmtpResponseBuilder newCommand() {
        return new SmtpResponseBuilder();
    }

    public SmtpResponseLineBuilder line() {
        return new SmtpResponseLineBuilder(this);
    }

    public SmtpResponseBuilder commandHandler(CommandHandler commandHandler) {
        this.commandHandler = commandHandler;

        return this;
    }

    public SmtpResponse build() {
        Objects.requireNonNull(this.commandHandler, "command handler not defined");

        SmtpResponse response = new SmtpResponse();
        response.setLines(this.lines);
        response.setCommandHandler(this.commandHandler);

        return response;
    }

    public static class SmtpResponseLineBuilder {

        private static final String CRLF = "\r\n";

        private static final String LF = "\n";

        private SmtpResponseBuilder smtpResponseBuilder;

        private SmtpResponseCode code;

        private String text;

        private String separator ;

        private boolean close;

        SmtpResponseLineBuilder(SmtpResponseBuilder smtpResponseBuilder) {
            this.smtpResponseBuilder  = smtpResponseBuilder;

            this.separator = CRLF;
            this.close = false;
        }

        public SmtpResponseLineBuilder code(SmtpResponseCode code) {
            this.code = code;

            return this;
        }

        public SmtpResponseLineBuilder text(String text) {
            this.text = text;

            return this;
        }

        public SmtpResponseLineBuilder crlf() {
            this.separator = CRLF;

            return this;
        }

        public SmtpResponseLineBuilder lf() {
            this.separator = LF;

            return this;
        }

        public SmtpResponseLineBuilder close(boolean close) {
            this.close = close;

            return this;
        }

        public SmtpResponseBuilder build() {
            Objects.requireNonNull(this.code, "Code not defined");
            Objects.requireNonNull(this.text, "Text not defind");

            SmtpResponseLine line = new SmtpResponseLine();
            line.setCode(this.code);
            line.setText(this.text);
            line.setClose(this.close);
            line.setSeparator(this.separator);

            this.smtpResponseBuilder.lines.add(line);

            return this.smtpResponseBuilder;
        }
    }
}
