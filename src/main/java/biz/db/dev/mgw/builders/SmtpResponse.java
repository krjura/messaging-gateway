package biz.db.dev.mgw.builders;

import biz.db.dev.mgw.handlers.CommandHandler;

import java.util.List;

public class SmtpResponse {

    private CommandHandler commandHandler;

    private List<SmtpResponseLine> lines;

    public CommandHandler getCommandHandler() {
        return commandHandler;
    }

    public void setCommandHandler(CommandHandler commandHandler) {
        this.commandHandler = commandHandler;
    }

    public List<SmtpResponseLine> getLines() {
        return lines;
    }

    void setLines(List<SmtpResponseLine> lines) {
        this.lines = lines;
    }
}
