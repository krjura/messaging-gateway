package biz.db.dev.mgw.builders;

import biz.db.dev.mgw.enums.SmtpResponseCode;

public class SmtpResponseLine {

    private SmtpResponseCode code;

    private String text;

    private String separator;

    private boolean close;

    SmtpResponseLine() {

    }

    public SmtpResponseCode getCode() {
        return code;
    }

    void setCode(SmtpResponseCode code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    void setText(String text) {
        this.text = text;
    }

    public String getSeparator() {
        return separator;
    }

    void setSeparator(String separator) {
        this.separator = separator;
    }

    public boolean isClose() {
        return close;
    }

    void setClose(boolean close) {
        this.close = close;
    }
}
