package biz.db.dev.mgw.utils;

import biz.db.dev.mgw.services.impl.SmtpServerHandlerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThreadBlocker implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(SmtpServerHandlerImpl.class);

    private boolean active;

    @Override
    public void run() {

        while (this.active) {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                logger.warn("sleep interrupted");

                Thread.currentThread().interrupt();
            }
        }

    }
}