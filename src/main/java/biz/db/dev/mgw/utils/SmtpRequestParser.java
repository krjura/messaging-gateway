package biz.db.dev.mgw.utils;

import biz.db.dev.mgw.smtp.SmtpRequest;

public class SmtpRequestParser {

    private SmtpRequestParser() {

    }

    public static SmtpRequest parse(String requestLine) {
        String command;
        String parameters = null;

        int i = requestLine.indexOf(' ');
        if (i > 0) {
            command = requestLine.substring(0, i).toUpperCase();
            parameters = requestLine.substring(i + 1);
        }
        else {
            command = requestLine.toUpperCase();
        }

        return new SmtpRequest(requestLine, command, parameters);
    }
}
