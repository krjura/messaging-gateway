package biz.db.dev.mgw.utils;

import biz.db.dev.mgw.builders.SmtpResponse;
import biz.db.dev.mgw.builders.SmtpResponseLine;
import biz.db.dev.mgw.smtp.SmtpSession;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class SmtpResponseUtils {

    private static final Logger logger = LoggerFactory.getLogger(SmtpResponseUtils.class);

    private static final String FIRST_LINES_SEPARATOR = "-";
    private static final String LAST_LINE_SEPARATOR = " ";

    private SmtpResponseUtils() {

    }

    public static void writeResponse(SmtpSession session, SmtpResponse response, ChannelHandlerContext ctx) {

        if(response.getLines().isEmpty()) {
            return;
        }

        int lineCount = response.getLines().size();

        for( int i = 0; i < lineCount; i++) {

            String separator = FIRST_LINES_SEPARATOR;

            if( i == lineCount - 1) {
                separator = LAST_LINE_SEPARATOR;
            }

            SmtpResponseLine line = response.getLines().get(i);

            String generatedLine = line.getCode().getCode() + separator + line.getText() + line.getSeparator();

            ChannelFuture future = ctx.write(generatedLine);

            if( line.isClose() ) {
                future.addListener(ChannelFutureListener.CLOSE);
            }

            if(logger.isDebugEnabled()) {
                logger.debug("'{}'", generatedLine);
                session.getServerLogger().writeServerOutgoingTraffic(generatedLine);
            }
        }
    }
}
