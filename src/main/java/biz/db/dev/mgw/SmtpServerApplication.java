package biz.db.dev.mgw;

import biz.db.dev.mgw.utils.ThreadBlocker;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableConfigurationProperties
@ComponentScan( basePackageClasses = {
        SmtpServerApplication.class
})
public class SmtpServerApplication {

    private SmtpServerApplication() {

    }

    public static void main(String[] args) {

        SpringApplication springApplication = new SpringApplication(SmtpServerApplication.class);
        springApplication.addListeners(new ApplicationPidFileWriter());
        springApplication.run(args);

        Thread blockerThread = new Thread(new ThreadBlocker());
        blockerThread.setDaemon(false);
        blockerThread.start();
    }
}
